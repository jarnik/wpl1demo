﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjectDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void WMain_Loaded(object sender, RoutedEventArgs e)
        {
            WMain.Background = new SolidColorBrush(Colors.LightGray);
        }

        private void ImgWilkens_bitter_MouseEnter(object sender, MouseEventArgs e)
        {
            Achtergrond.Background = Brushes.Yellow;
        }

        private void ImgRoos_MouseEnter(object sender, MouseEventArgs e)
        {
            Achtergrond.Background = Brushes.Red;
        }

        private void ImgGoudsBloem_MouseEnter(object sender, MouseEventArgs e)
        {
            Achtergrond.Background = Brushes.Orange;
        }

        private void ImgLavender_MouseEnter(object sender, MouseEventArgs e)
        {
            Achtergrond.Background = Brushes.Purple;
        }

        private void ImgLavender_MouseLeave(object sender, MouseEventArgs e)
        {
            Achtergrond.Background = Brushes.Gray;
        }

        private void ImgWilkens_bitter_MouseLeave(object sender, MouseEventArgs e)
        {
            Achtergrond.Background = Brushes.Gray;
        }

        private void ImgGoudsBloem_MouseLeave(object sender, MouseEventArgs e)
        {
            Achtergrond.Background = Brushes.Gray;
        }

        private void ImgRoos_MouseLeave(object sender, MouseEventArgs e)
        {

        }
    }
}
